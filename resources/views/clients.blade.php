@extends('layouts.client')

@section('content')
<section class="banner-inner-sec" style="background-image:url('assets/images/our-project/banner.jpg')">
	<div class="banner-table">
		<div class="banner-table-cell">
			<div class="container">
				<div class="banner-inner-content">
					<h2 class="banner-inner-title">{{ __('Clientes') }}</h2>
					<!--<ul class="xs-breadcumb">
						<li><a href="index.html"> Inicio  / </a>  clientes</li>
					</ul>-->
				</div>
			</div>
		</div>
	</div>
</section>
<!--breadcumb end here-->

<!-- start tips and tricks section -->
<section class="tips-tricks-sec section-padding">
        <div class="container">
    
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-tips-tricks tips-tricks-item">
                        <img src="assets/images/tips-img2.jpg" alt="">
                        <div class="tips-tricks-content">
                            <span>{{ __('nt_cliente_1_fecha') }}</span>
                            <h4>{{ __('nt_cliente_1') }}</h4>
                            <a href="#popup_1" class="read_more xs-image-popup" data-effect="mfp-zoom-in">{{ __('nt_leer_mas') }}<i class="icon icon-arrow-right"></i> </a>
                            <div id="popup_1" class="container xs-gallery-popup-item mfp-hide"><!--inicio del popup-->
                                <div class="row">
                                    <div class="col-lg-5 xs-padding-0">
                                        <div class="xs-popup-img">
                                            <img src="assets/images/our-project/project_1.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="xs-popup-content">
                                            <h2 class="hidden-title">{{__('nt_info_title_client')}}</h2>
                                            <h3>{{__('nt_cliente_1')}}</h3>
                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <ul class="xs-popup-left-content">
                                                        <li>
                                                            <i class="icon icon-calendar-full"></i>
                                                            <label>{{__('nt_label_client_date_1')}}</label>
                                                            <span>{{__('nt_info_client_date_1')}}</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-tags"></i>
                                                            <label>{{__('nt_label_client_category_1')}}</label>
                                                            <span>Tostado,</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-user2"></i>
                                                            <label>{{__('nt_label_client_name_1')}}</label>
                                                            <span>Mr. Jordan, Alemania</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-invest"></i>
                                                            <label>{{__('nt_label_client_price_1')}}</label>
                                                            <span>$ 500</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-map-marker2"></i>
                                                            <label>{{__('nt_label_client_adress_1')}}</label>
                                                            <span>Alemania,
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-lg-7">
                                                    <div class="xs-popup-right-content">
                                                            <p>
                                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                                    tempor incididunt ut labore et dolore magna aliqua.
                                                            </p>
                                                            <blockquote>
                                                                “lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                                tempor incididunt ut labore et dolore magna aliqua.”
                                                            </blockquote>
                                                            <p>
                                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                                    tempor incididunt ut labore et dolore magna aliqua..
                                                            </p>
                                                            <a href="#" class="xs-btn">LINK DEL PROYECTO</a>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--fin del popup-->
                        </div>
                        <div class="tag-line">{{ __('nt_cliente_1') }}</div>
                    </div>
                </div>
    
                <div class="col-lg-3 col-md-6">
                    <div class="single-tips-tricks tips-tricks-item">
                        <img src="assets/images/tips-img2.jpg" alt="">
                        <div class="tips-tricks-content">
                            <span>{{ __('nt_cliente_2_fecha') }}</span>
                            <h4>{{ __('nt_cliente_2') }}</h4>
                            <a href="#popup_2" class="read_more xs-image-popup" data-effect="mfp-zoom-in">{{ __('nt_leer_mas') }}<i class="icon icon-arrow-right"></i> </a>
                            <div id="popup_2" class="container xs-gallery-popup-item mfp-hide"><!--inicio del popup-->
                                <div class="row">
                                    <div class="col-lg-5 xs-padding-0">
                                        <div class="xs-popup-img">
                                            <img src="assets/images/our-project/project_1.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="xs-popup-content">
                                            <h2 class="hidden-title">{{__('nt_info_title_client')}}</h2>
                                            <h3>{{__('nt_cliente_2')}}</h3>
                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <ul class="xs-popup-left-content">
                                                        <li>
                                                            <i class="icon icon-calendar-full"></i>
                                                            <label>{{__('nt_label_client_date_2')}}</label>
                                                            <span>{{__('nt_info_client_date_2')}}</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-tags"></i>
                                                            <label>{{__('nt_label_client_category_2')}}</label>
                                                            <span>Tostado,</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-user2"></i>
                                                            <label>{{__('nt_label_client_name_2')}}</label>
                                                            <span>Mr. Jordan, Alemania</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-invest"></i>
                                                            <label>{{__('nt_label_client_price_2')}}</label>
                                                            <span>$ 500</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-map-marker2"></i>
                                                            <label>{{__('nt_label_client_adress_2')}}</label>
                                                            <span>Alemania,
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-lg-7">
                                                    <div class="xs-popup-right-content">
                                                            <p>
                                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                                    tempor incididunt ut labore et dolore magna aliqua.
                                                            </p>
                                                            <blockquote>
                                                                “lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                                tempor incididunt ut labore et dolore magna aliqua.”
                                                            </blockquote>
                                                            <p>
                                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                                    tempor incididunt ut labore et dolore magna aliqua..
                                                            </p>
                                                            <a href="#" class="xs-btn">LINK DEL PROYECTO</a>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--fin del popup-->
                        </div>
                        <div class="tag-line">{{ __('nt_cliente_2') }}</div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-tips-tricks tips-tricks-item">
                        <img src="assets/images/tips-img3.jpg" alt="">
                        <div class="tips-tricks-content">
                            <span>{{ __('nt_cliente_3_fecha') }}</span>
                            <h4>{{ __('nt_cliente_3') }}</h4>
                            <a href="#popup_3" class="read_more xs-image-popup" data-effect="mfp-zoom-in">{{ __('nt_leer_mas') }}<i class="icon icon-arrow-right"></i> </a>
                            <div id="popup_3" class="container xs-gallery-popup-item mfp-hide"><!--inicio del popup-->
                                <div class="row">
                                    <div class="col-lg-5 xs-padding-0">
                                        <div class="xs-popup-img">
                                            <img src="assets/images/our-project/project_1.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="xs-popup-content">
                                            <h2 class="hidden-title">{{__('nt_info_title_client')}}</h2>
                                            <h3>{{__('nt_cliente_3')}}</h3>
                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <ul class="xs-popup-left-content">
                                                        <li>
                                                            <i class="icon icon-calendar-full"></i>
                                                            <label>{{__('nt_label_client_date_3')}}</label>
                                                            <span>{{__('nt_info_client_date_3')}}</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-tags"></i>
                                                            <label>{{__('nt_label_client_category_3')}}</label>
                                                            <span>Tostado,</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-user2"></i>
                                                            <label>{{__('nt_label_client_name_3')}}</label>
                                                            <span>Mr. Jordan, Alemania</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-invest"></i>
                                                            <label>{{__('nt_label_client_price_3')}}</label>
                                                            <span>$ 500</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-map-marker2"></i>
                                                            <label>{{__('nt_label_client_adress_3')}}</label>
                                                            <span>Alemania,
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-lg-7">
                                                    <div class="xs-popup-right-content">
                                                        <p>
                                                                lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                                tempor incididunt ut labore et dolore magna aliqua.
                                                        </p>
                                                        <blockquote>
                                                            “lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                            tempor incididunt ut labore et dolore magna aliqua.”
                                                        </blockquote>
                                                        <p>
                                                                lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                                tempor incididunt ut labore et dolore magna aliqua..
                                                        </p>
                                                        <a href="#" class="xs-btn">LINK DEL PROYECTO</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--fin del popup-->
                        </div>
                        <div class="tag-line">{{ __('nt_cliente_3') }}</div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-tips-tricks tips-tricks-item">
                        <img src="assets/images/tips-img2.jpg" alt="">
                        <div class="tips-tricks-content">
                            <span>{{ __('nt_cliente_4_fecha') }}</span>
                            <h4>{{ __('nt_cliente_4') }}</h4>
                            <a href="#popup_4" class="read_more xs-image-popup" data-effect="mfp-zoom-in">{{ __('nt_leer_mas') }}<i class="icon icon-arrow-right"></i> </a>
                            <div id="popup_4" class="container xs-gallery-popup-item mfp-hide"><!--inicio del popup-->
                                <div class="row">
                                    <div class="col-lg-5 xs-padding-0">
                                        <div class="xs-popup-img">
                                            <img src="assets/images/our-project/project_1.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="xs-popup-content">
                                            <h2 class="hidden-title">{{__('nt_info_title_client')}}</h2>
                                            <h3>{{__('nt_cliente_4')}}</h3>
                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <ul class="xs-popup-left-content">
                                                        <li>
                                                            <i class="icon icon-calendar-full"></i>
                                                            <label>{{__('nt_label_client_date_4')}}</label>
                                                            <span>{{__('nt_info_client_date_4')}}</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-tags"></i>
                                                            <label>{{__('nt_label_client_category_4')}}</label>
                                                            <span>Tostado,</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-user2"></i>
                                                            <label>{{__('nt_label_client_name_4')}}</label>
                                                            <span>Mr. Jordan, Alemania</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-invest"></i>
                                                            <label>{{__('nt_label_client_price_4')}}</label>
                                                            <span>$ 500</span>
                                                        </li>
                                                        <li>
                                                            <i class="icon icon-map-marker2"></i>
                                                            <label>{{__('nt_label_client_adress_4')}}</label>
                                                            <span>Alemania,
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-lg-7">
                                                    <div class="xs-popup-right-content">
                                                        <p>
                                                                lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                                tempor incididunt ut labore et dolore magna aliqua.
                                                        </p>
                                                        <blockquote>
                                                            “lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                            tempor incididunt ut labore et dolore magna aliqua.”
                                                        </blockquote>
                                                        <p>
                                                                lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                                tempor incididunt ut labore et dolore magna aliqua..
                                                        </p>
                                                        <a href="#" class="xs-btn">LINK DEL PROYECTO</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--fin del popup-->
                        </div>
                        <div class="tag-line">{{ __('nt_cliente_4') }}</div>
                    </div>
                </div>
            </div>
        </div><!-- .container end -->
    </section><!-- End tips and tricks section -->

<!-- header ready to contact section -->
<section class="ready-to-contact section-padding" style="background: url(./assets/images/about/ready-to-contact.jpg) no-repeat center center /cover">
    <div class="container">
       <div class="col-lg-8 offset-lg-2">
           <div class="ready-to-contact-content">
               <h2>{{ __('nt_estas_listo') }}</h2>
               <p>{{ __('nt_estas_listo_texto') }}
                </p>
                <a href="#" class="xs-btn fill">{{ __('Contactenos') }}</a>
           </div>
       </div>
    </div><!-- .container end -->
</section>
@endsection